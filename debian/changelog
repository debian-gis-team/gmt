gmt (6.5.0+dfsg-4) unstable; urgency=medium

  * Add patch by wuruilong to fix FTBFS on loong64.
    (closes: #1068614)
  * Update watch file to use GitHub tags.
  * Bump Standards-Version to 4.7.0, no changes.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 06 Mar 2025 18:42:57 +0100

gmt (6.5.0+dfsg-3) unstable; urgency=medium

  * Add dpkg-dev (>= 1.22.5) to build dependencies for t64 changes.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 02 Mar 2024 12:45:37 +0100

gmt (6.5.0+dfsg-2) unstable; urgency=medium

  * Replace pkg-config build dependency with pkgconf.
  * Move from experimental to unstable.
    (closes: #1062177)

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 28 Feb 2024 15:31:44 +0100

gmt (6.5.0+dfsg-2~exp1) experimental; urgency=medium

  * Improve t64 changes to match team standards.
  * Fix lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 05 Feb 2024 13:56:47 +0100

gmt (6.5.0+dfsg-1.1~exp1) experimental; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.

 -- Lukas Märdian <slyon@debian.org>  Wed, 31 Jan 2024 15:18:42 +0000

gmt (6.5.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.2, no changes.
  * Bump debhelper compat to 13.
  * Use execute_after instead of override in rules file.
  * Enable Salsa CI.
  * Update copyright file.
  * Refresh patches.
  * Include gsfml executables in gmt binary package.
  * Update symbols for 6.5.0.
  * Update lintian overrides.
  * Add patch to fix spelling errors.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 08 Jan 2024 05:48:05 +0100

gmt (6.4.0+dfsg-2) unstable; urgency=medium

  * Bump Standards-Version to 4.6.1, no changes.
  * Drop obsolete dh_strip override, dbgsym migration complete.
  * Add Rules-Requires-Root to control file.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 01 Dec 2022 09:33:35 +0100

gmt (6.4.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Exclude PCRE to prefer PCRE2 (pcre2 pulled in via dependencies).
  * Update copyright file.
  * Drop bswap32.patch, applied upstream. Refresh remaining patches.
  * Fix (un)used substitution variables.
  * Update symbols for 6.4.0.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 18 Jun 2022 17:33:16 +0200

gmt (6.3.0+dfsg-2) unstable; urgency=medium

  * Add patch to fix FTBFS on s390x.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 20 Nov 2021 08:09:28 +0100

gmt (6.3.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.0, no changes.
  * Bump debhelper compat to 12, changes:
    - Drop --list-missing from dh_install
  * Update lintian overrides.
  * Add pkg-config to build dependencies.
  * Refresh patches.
  * Update symbols for 6.3.0.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 20 Nov 2021 06:19:12 +0100

gmt (6.2.0+dfsg-1) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 15 Aug 2021 10:17:41 +0200

gmt (6.2.0+dfsg-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Strip pre-releases from symbols version.
  * Update symbols for 6.2.0.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 06 Jun 2021 07:04:28 +0200

gmt (6.2.0~rc2+dfsg-1~exp1) experimental; urgency=medium

  * New upstream release candidate.
  * Refresh patches.
  * Update symbols for 6.2.0~rc2.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 25 May 2021 05:53:56 +0200

gmt (6.2.0~rc1+dfsg-1~exp1) experimental; urgency=medium

  * New upstream release candidate.
  * Bump watch file version to 4.
  * Update lintian overrides.
  * Bump Standards-Version to 4.5.1, no changes.
  * Require at least gmt-dcw 2.0.0.
  * Update copyright file.
  * Refresh patches.
  * Update symbols for 6.2.0~rc1.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 13 Apr 2021 12:12:43 +0200

gmt (6.1.1+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update symbols for 6.1.1.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 03 Sep 2020 05:56:31 +0200

gmt (6.1.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
    (closes: #958556)
  * Update copyright file.
  * Refresh patches.
  * Update symbols for 6.1.0.
  * Add overrides for executable-not-elf-or-script & national-encoding.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 05 Jul 2020 07:50:41 +0200

gmt (6.0.0+dfsg-2) unstable; urgency=medium

  * Drop Name field from upstream metadata.
  * Bump Standards-Version to 4.5.0, no changes.
  * Bump debhelper compat to 10, changes:
    - Drop --parallel option, enabled by default
  * Add upstream patch to fix FTBFS with gcc-10.
    (closes: #957277)
  * Add lintian override for manpage-without-executable.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 17 Apr 2020 20:56:33 +0200

gmt (6.0.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Install upstream manpages.
  * Refresh patches.
  * Strip pre-releases from symbols version.
  * Drop unused override for sphinxdoc-but-no-sphinxdoc-depends.
  * Update symbols for 6.0.0.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 01 Nov 2019 21:15:10 +0100

gmt (6.0.0~rc5+dfsg1-1~exp1) experimental; urgency=medium

  * New upstream release candidate.
  * Bump Standards-Version to 4.4.1, no changes.
  * Update copyright file.
  * Refresh patches.
  * Update lintian override for manpages, no longer provided by upstream.
  * Update symbols for 6.0.0~rc5.
  * Repack upstream tarball to exclude rst sources.
  * Drop gmt-docs package.
  * Drop obsolete dh_installman override.
  * Drop gmt-examples package.
  * Add lintian override for file-references-package-build-path.
  * Include supplements in gmt-common package.
  * Add lintian override for sphinxdoc-but-no-sphinxdoc-depends.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 24 Oct 2019 09:54:15 +0200

gmt (6.0.0~rc4+dfsg-1~exp1) experimental; urgency=medium

  * New upstream release candidate.
  * Refresh patches.
  * Drop python-sphinx from build dependencies.
  * Update symbols for 6.0.0~rc4.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 07 Sep 2019 08:35:33 +0200

gmt (6.0.0~rc3+dfsg-1~exp1) experimental; urgency=medium

  * New upstream release candidate.
  * Update gbp.conf to use --source-only-changes by default.
  * Bump Standards-Version to 4.4.0, no changes.
  * Drop spelling-errors.patch, applied upstream.
  * Update symbols for 6.0.0~rc3.
  * Drop unused override for pkg-js-tools-test-is-missing.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 16 Jul 2019 06:03:19 +0200

gmt (6.0.0~rc2+dfsg-1~exp1) experimental; urgency=medium

  * New upstream release candidate.
  * Refresh patches.
  * Update symbols for 6.0.0~rc2.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 04 Jul 2019 06:18:13 +0200

gmt (6.0.0~rc1+dfsg-1~exp1) experimental; urgency=medium

  * New upstream release candidate.
    (closes: #930082)
  * Fix uversionmangle for pre-releases in watch file.
  * Drop patches applied/included upstream. Refresh remaining patches.
  * Update copyright file, changes:
    - Update copyright years for copyright holders
    - Drop license & copyright for getopt sources, removed upstream
    - Add license & copyright for cpt & mergesort sources
  * Add additional build dependencies (gdal-bin, ffmpeg, graphicsmagick,
    libblas-dev, liblapackdev).
  * Don't remove executable bit from example files, no longer built.
  * Don't install tutorial in gmt-doc, no longer built.
  * Rename library package for SONAME bump.
  * Use libjs-mathjax to fix privacy-breach-generic issues.
  * Update symbols for 6.0.0~rc1.
  * Add patch to fix spelling errors.
  * Add lintian override for pkg-js-tools-test-is-missing false positive.
  * Don't install empty pdf directory in gmt-doc.
  * Add patch to fix incorrect-path-for-interpreter issue.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 20 Jun 2019 08:40:17 +0200

gmt (5.4.5+dfsg-2) unstable; urgency=medium

  * Remove package name from lintian overrides.
  * Add upstream patch to fix segfault with NetCDF 4.6.3.
    (closes: #929264)

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 21 May 2019 09:39:24 +0200

gmt (5.4.5+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.3.0, no changes.
  * Update copyright years for copyright holders.
  * Update upstream metadata for move to GitHub.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 05 Jan 2019 08:50:08 +0100

gmt (5.4.4+dfsg-3) unstable; urgency=medium

  * Bump Standards-Version to 4.2.1, no changes.
  * Add Build-Depends-Package field to symbols file.
  * Change build dependency from libpcre3-dev to libpcre2-dev.
    (closes: #911934)
  * Add patch to fix spelling errors.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 26 Oct 2018 16:07:29 +0200

gmt (5.4.4+dfsg-2) unstable; urgency=medium

  * Bump Standards-Version to 4.1.5, no changes.
  * Drop autopkgtest to test installability.
  * Add lintian override for testsuite-autopkgtest-missing.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 31 Jul 2018 16:27:07 +0200

gmt (5.4.4+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright-format URL to use HTTPS.
  * Fix deprecated source override location.
  * Update Vcs-* URLs for Salsa.
  * Bump Standards-Version to 4.1.4, no changes.
  * Strip trailing whitespace from control & rules files.
  * Refresh patches.
  * Fix bash-completion installation.
  * Update symbols for 5.4.4.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 01 Jul 2018 08:57:31 +0200

gmt (5.4.3+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright years for copyright holders.
  * Update symbols for 5.4.3.
  * Bump Standards-Version to 4.1.3, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 03 Jan 2018 08:06:26 +0100

gmt (5.4.2+dfsg-2) unstable; urgency=medium

  * Change priority from extra to optional.
  * Bump Standards-Version to 4.1.2, changes: priority.
  * Add python3-sphinx to build dependencies.
  * Strip trailing whitespace from changelog & rules.
  * Add some Multi-Arch fields suggested by Multiarch hinter.
  * Drop obsolete dbg package.
  * Add lintian override for debian-watch-uses-insecure-uri.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 26 Dec 2017 19:59:39 +0100

gmt (5.4.2+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Drop m68k-support.patch, applied upstream.
    Refresh manpage-section.patch.
  * Update symbols for 5.4.2.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 27 Jun 2017 00:28:47 +0200

gmt (5.4.1+dfsg-3) unstable; urgency=medium

  * Fix SOURCE_DATE_EPOCH use.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 26 Jun 2017 20:44:49 +0200

gmt (5.4.1+dfsg-2) unstable; urgency=medium

  * Add patch by John Paul Adrian Glaubitz for m68k support.
    (closes: #865726)
  * Bump Standards-Version to 4.0.0, no changes.
  * Add autopkgtest to test installability.
  * Use pkg-info.mk variables instead of dpkg-parsechangelog output.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 24 Jun 2017 11:31:13 +0200

gmt (5.4.1+dfsg-1) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 18 Jun 2017 10:44:23 +0200

gmt (5.4.1+dfsg-1~exp1) experimental; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 10 May 2017 07:34:20 +0200

gmt (5.4.0+dfsg-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Add libcurl4-gnutls-dev | libcurl-ssl-dev to build dependencies.
  * Refresh patches.
  * Update symbols for 5.4.0.
  * Drop gmt_url.c from copyright file, removed upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 09 May 2017 07:53:39 +0200

gmt (5.3.3+dfsg-1~exp1) experimental; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 23 Mar 2017 22:53:33 +0100

gmt (5.3.2+dfsg-1~exp2) experimental; urgency=medium

  * Move supplements plugin from libgmt-dev to gmt package.
    See: http://gmt.soest.hawaii.edu/issues/972#note-8

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 20 Mar 2017 21:21:22 +0100

gmt (5.3.2+dfsg-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Update copyright years for copyright holders.
  * Drop spelling-errors.patch, applied upstream.
  * Update symbols for 5.3.2.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 26 Feb 2017 12:11:15 +0100

gmt (5.3.1+dfsg-2) unstable; urgency=medium

  * Use dh_auto_{clean,build,install} instead of custom commands.
  * Drop disable-examples.patch, no longer causes FTBFS.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 05 Nov 2016 13:27:49 +0100

gmt (5.3.1+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update Homepage URL to not include 'projects/gmt' path.
  * Update copyright file, changes:
    - Update copyright years for copyright holders
    - Add Dongdong Tian to copyright holders
  * Drop patches applied upstream. Refresh remaining patches.
  * Drop custom coastline.conf, not installed.
  * Update gmt-config manpage for upstream changes.
  * Update symbols for version 5.3.1.
  * Add patch to fix spelling errors.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 22 Oct 2016 13:03:41 +0200

gmt (5.2.1+dfsg-7) unstable; urgency=medium

  * Add patch to fix FTBFS with glibc 2.23.
    (closes: #818807)

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 07 Jul 2016 18:13:57 +0200

gmt (5.2.1+dfsg-6) unstable; urgency=medium

  * Add patch to use SOURCE_DATE_EPOCH instead of current date.
    (closes: #824668)

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 28 May 2016 12:39:02 +0200

gmt (5.2.1+dfsg-5) unstable; urgency=medium

  * Bump Standards-Version to 3.9.8, no changes.
  * Enable all hardening buildflags.
  * Add patch for spelling errors.
  * Drop obsolete lintian override for old-style-config-script-multiarch-path.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 05 May 2016 12:11:06 +0200

gmt (5.2.1+dfsg-4) unstable; urgency=medium

  * Update Vcs-Git URL to use HTTPS.
  * Bump Standards-Version to 3.9.7, no changes.
  * Add patches for various typos.
  * Add lintian override for old-style-config-script-multiarch-path.
    See: https://lists.debian.org/debian-devel/2016/01/msg00688.html
  * Drop unused FLOCK CMake option.
  * Add patch to fix build on sparc64.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 19 Feb 2016 02:43:47 +0100

gmt (5.2.1+dfsg-3) unstable; urgency=medium

  * Add patch to fix build on HPPA.
    Thanks to John David Anglin for the patch.
    (closes: #805731)
  * Add patch to fix build on alpha.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 21 Nov 2015 20:10:21 +0100

gmt (5.2.1+dfsg-2) unstable; urgency=medium

  * Add Breaks/Replaces for files moved from gmt-doc-pdf to gmt-doc.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 15 Nov 2015 21:20:23 +0100

gmt (5.2.1+dfsg-1) unstable; urgency=medium

  * Move from experimental to unstable

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 14 Nov 2015 13:06:10 +0100

gmt (5.2.1+dfsg-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Update copyright file, changes:
    - Add license & copyright for gmt_url.c
    - Drop license & copyright for removed compat headers
  * Drop patches applied upstream, refresh remaining patches.
  * Install PDF & tutorial files in gmt-doc too.
  * Update symbols for 5.2.1.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 13 Nov 2015 11:06:30 +0100

gmt (5.1.2+dfsg1-3) unstable; urgency=medium

  * Apply patch by Andreas Tille to fix YAML syntax for upstream metadata.
  * Add patch for 'below' typo.
  * Rebuild for gdal transition.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 22 Oct 2015 10:10:41 +0200

gmt (5.1.2+dfsg1-2) unstable; urgency=medium

  * Override dh_auto_configure to use custom CMake options.
  * Drop changes for HDF5 MPI, netcdf now built with serial flavor.
  * Add patch to fix _netcdf_dashl regex in FindNETCDF.
  * Don't build PDF documentation, latex build is prone to failure.
  * Add patches for various typos.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 24 Aug 2015 18:43:16 +0200

gmt (5.1.2+dfsg1-2~exp1) experimental; urgency=medium

  * Add patch to find the HDF5 library required for NetCDF in experimental.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 29 Jul 2015 22:30:36 +0200

gmt (5.1.2+dfsg1-1) unstable; urgency=medium

  * Add patch rename psl library to gmt-psl to resolve conflict with
    libpsl.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 23 Jul 2015 00:49:03 +0200

gmt (5.1.2+dfsg1-1~exp8) experimental; urgency=medium

  * Drop Conflicts on turnin-ng & pslib-dev,
    gmt man pages use 'gmt' suffix now.
  * Add Breaks/Replaces on old gmt-manpages to gmt-common.
    (closes: #790524)
  * Drop obsolete README.Debian file.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 10 Jul 2015 16:33:55 +0200

gmt (5.1.2+dfsg1-1~exp7) experimental; urgency=medium

  * Add patch to append 'gmt' to the man page sections.
  * Move `gzip -n` man page compression from rules to patch.
  * Don't use cmake continue() command in disable-examples.patch,
    requires CMake >= 3.2.
  * Drop libgmt-dev.links, usr/lib/gmt is no longer used.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 09 Jul 2015 22:39:30 +0200

gmt (5.1.2+dfsg1-1~exp6) experimental; urgency=medium

  * Add Breaks/Replaces on libgenome-perl versions preceding the gmt rename.
    (closes: #790524)

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 09 Jul 2015 08:13:17 +0200

gmt (5.1.2+dfsg1-1~exp5) experimental; urgency=medium

  * Add versioned Breaks/Replaces on gmt to gmt-common.
    (closes: #790521)
  * Add Conflicts on turnin-ng & pslib-dev to gmt-common,
    and libpsl-dev to libgmt-dev.
  * Add references to upstream metadata.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 03 Jul 2015 19:30:54 +0200

gmt (5.1.2+dfsg1-1~exp4) experimental; urgency=medium

  * Add patch to fix build on mips*.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 27 Jun 2015 15:53:32 +0200

gmt (5.1.2+dfsg1-1~exp3) experimental; urgency=medium

  * Only recompress man pages if they exist.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 26 Jun 2015 21:07:10 +0200

gmt (5.1.2+dfsg1-1~exp2) experimental; urgency=medium

  * Fix removal of empty directory.
  * Add patch to fix build on kFreeBSD.
  * Add upstream metadata.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 26 Jun 2015 19:36:59 +0200

gmt (5.1.2+dfsg1-1~exp1) experimental; urgency=medium

  [ Francesco Paolo Lovergine ]
  * New upstream major release: build system moved to cmake.
  * Now following general advice from upstream about packaging as listed
    in http://gmt.soest.hawaii.edu/projects/gmt/wiki/PackagingGMT
  * Added a new cmake patch with Debian changes to CMake configurations.
  * Removed autotools b-dep and rules.
  * Removed all (obsolete) patches.
  * Moved to current full dh_* auto support.
  * Octave/mex stuff now lives in its own repository and should go into
    a separate source package. So, dropped octave-gmt ATM.
  * Updated list of b-d.
  * Set Vcs-* fields to canonical forms.

  [ Ross Gammon ]
  * Update watch file with sepwatch changes.

  [ Bas Couwenberg ]
  * New upstream release.
  * Add myself to Uploaders.
  * Update watchfile, changes:
    - Use passive FTP
    - Add common dversionmangle
  * Restructure control file with cme, changes:
    - Update Vcs-Browser URL to use cgit instead of gitweb
    - Bump Standards-Version to 3.9.6, no changes.
  * Add gbp.conf to use pristine-tar by default.
  * Update copyright file, changes:
    - Changed GMT license to LGPL-3+ only
    - Update copyright years, add additional copyright holders
    - Add license & copyright for differently licensed files (mostly BSD/MIT)
  * Repack upstream source using Files-Excluded in the copyright file.
  * Fix libgmt5 installation.
  * Disable use of non-free triangle source.
  * Fix bash-completion installation.
  * Strip RPATH from binary & libraries.
  * Also exclude prebuilt documentation and manpages from repacked source.
  * Build documentation in build-indep target.
  * Use packaged jquery.js & underscore.js instead of embedded sphinx copies.
  * Move /usr/share/gmt to architecture-independent gmt-common package.
  * Recompress the manpages to not use timestamped gzip.
  * Add patches for various typos.
  * Add symbols file for libgmt5.
  * Add debug package.
  * Use Breaks/Replaces instead of versioned Conflicts.
  * Add manpage for gmt-config.
  * Drop gmt-tutorial-pdf package, no longer included upstream.
  * Use libfftw3-dev instead of sfftw-dev.
  * Update dependencies for new gmt-dcw & gmt-gshhg data packages.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 20 Jun 2015 17:14:18 +0200

gmt (4.5.12-1) unstable; urgency=medium

  * New upstream release for 4.x series, bug fixing only.
  * Now using unversioned paths for installing the octave files.
    Thanks Rafael Laboissiere (closes: #734047).
  * Policy set to 3.9.5, no changes done.
  * Set debhelper to level 9 for both -arch and -indep sections.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Thu, 20 Mar 2014 10:34:32 +0100

gmt (4.5.11-1) unstable; urgency=low

  * New upstream release for the 4.x series.
  * Added patch gmt_io.c, mgd77header.c and gmt_proj.c to avoid nested comments
    use while -pedantic mode is enabled.
  * Added patch c++comments to remove C++ style comments in pure C code. This
    is currently deprecated and should be avoided when strict option
    like -pedantic are used.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Mon, 25 Nov 2013 15:57:54 +0100

gmt (4.5.9-1) unstable; urgency=low

  * Added Octave support and an ad-hoc new package for that.
  * Added octave-gmt package for supporting grid files in GNU octave.
  * Revised build-deps.
  * Debhelper level set to 9.
  * Lintian-based cleanups:
      - Fixed debian/changelog for a couple of silly warnings.
      - Added ${misc:Depends} to all pkgs.
  * Revised for Suggestions.
  * Fixed previous changelog for layout and contents.
  * ACK of previous NMUi by Sebastian Ramacher:
  	* debian/patches/eglibc-2.17.patch: Add missing include to fix FTBFS with
      eglibc 2.17.
    (closes: #701431)
  * New upstream release.
    (closes: #705022)
  * Patch kfreebsd refreshed to include hurd case instead.
    (closes: #676147)

 -- Francesco Paolo Lovergine <frankie@debian.org>  Fri, 16 Mar 2012 14:53:42 +0100

gmt (4.5.7-2) unstable; urgency=low

  * A link moved from gmt to gmt-doc, so conflicting current gmt-doc with
    old gmt.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Fri, 16 Mar 2012 14:14:25 +0100

gmt (4.5.7-1) unstable; urgency=low

  * New upstream version with a new compact distribution.
    (closes: #663099)
  * Policy bumped to 3.9.3, no changes required.
  * Now use in embedded mode quilt and all tar/unpack/patch target have been
    dropped.
  * Get-orig-source and all other special target dropped due to simplification
    of the distribution.
  * Added a build-dep on autotools-dev and provided usual snippets to override
    config.{sub,guess} files before configuring.
  * Moved to copyright format version 1.0. Note that in 4.5.6 GMT team added
    compatibility with any later version for GPL-2.
  * Split links helper in different files for each package to avoid
    unresolved links.
  * This package is compatible with GSHHS >= 2.2, as provided by a new separate
    package. Note that upstream no more distributes old datasets, so any
    previous package cannot use anymore high/full resolutions.
  * Fixed some symlinks.
    (closes: #343383)
  * Added a patch kfreebsd to allow working with GNU/kFreeBSD.
    (closes: #619960)
  * Fixed bashism in psbbox.sh with patch bashism.
    (closes: #581103)

 -- Francesco Paolo Lovergine <frankie@debian.org>  Tue, 13 Mar 2012 16:40:33 +0100

gmt (4.5.6-1) unstable; urgency=low

  * New upstream release.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Mon, 28 Mar 2011 12:50:08 +0200

gmt (4.5.5-1) unstable; urgency=low

  * New upstream release.
  * Moved from svn to git repository and changed Vcs-* as due.
  * Policy bumped to 3.9.1.
  * Compatibility level set to 8.
  * Now using source format 3.0. Note that quilt is still required
    as explicit build-dep due to the peculiar unpacking method.
  * Fixed creat-orig-tar to create the use the correct tarball name.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Mon, 07 Mar 2011 15:44:04 +0100

gmt (4.5.2-1) unstable; urgency=low

  * New upstream release.
  * Policy bumped to 3.8.4. No changes.
  * Added a NEWS file about new coastline data format.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Mon, 15 Feb 2010 18:09:07 +0100

gmt (4.5.1-1) unstable; urgency=low

  * New upstream release.
  * Policy bumped to 3.8.3. No changes.
  * [lintian] Changed my dotted name in Uploaders to avoid annoying warnings.
  * [lintian] Wrapped changelog line.
  * Fixed creat-orig-tar rule in debian/rules for correct directory naming.
  * Added ${misc:Depends} item in debian/control.
  * GMT uses GPL-2 strictly.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Mon, 21 Sep 2009 22:47:01 +0200

gmt (4.4.0-1) unstable; urgency=low

  * New upstream release. Note that /etc/gmt/gmt_formats.conf is no more
    installed and all provided files are removed.
    (closes: #490268, #499648)
  * Moved to debhelper level 7.
  * Added a dh_prep call at building start.
  * Revised debian/rules to work with new upstream locations.
  * Updated README.Debian.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Thu, 26 Mar 2009 11:19:15 +0100

gmt (4.3.1-3) unstable; urgency=medium

  * Removed headers from gmt binary package.
    (closes: #488779)

 -- Francesco Paolo Lovergine <frankie@debian.org>  Tue, 01 Jul 2008 17:53:56 +0200

gmt (4.3.1-2) unstable; urgency=low

  * First upload to unstable.
  * gmt-examples section set to doc.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Fri, 27 Jun 2008 10:01:49 +0200

gmt (4.3.1-1) experimental; urgency=low

  * New upstream release
    (closes: #480931)
  * Fixed download URL.
  * Added a README.source file to document how creating the upstream
    tarball.
  * Patch disable_unsupported.diff dropped and moved to use appropriate
    configure flag.
  * Added three new PDF documents in different formats.
  * Policy bumped to 3.8.0.
  * Now GMT is providing correct versioning and SONAMEs for shared libs,
    so moving to supporting development libraries by splitting libgmt4 and
    libgmt-dev binary packages. A long due wishlist finally managed, yeah!
    (closes: #210611)
  * Removed manpages duplicates and changing man section to *gmt
    in man headers [lintian].
  * Removed empty man directories.
  * Fixed clean target to remove GMT symlink.
  * Fixed wrapping in debian/changelog [lintian]
  * Added copyright notice to debian/copyright [lintian]
  * Moved to libnetcdf-dev instead of old netcdfg-dev now obsolete.
  * Build-dep on c-shell removed, because not more required.
  * Long description revised.
  * CC_OPT is not more used in building, uses CFLAGS instead.
  * Added symlink to all *.conf files in /etc within /usr/share/gmt.
  * Fixed debian/gmt-examples.examples to install examples for true.
  * Now avoids compression of examples/ stuff in debian/rules.
  * A note has been added to README.Debian to explain how to run examples
    scripts.
  * Added debian/watch file.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Mon, 16 Jun 2008 14:16:11 +0200

gmt (4.2.1-1) unstable; urgency=low

  * New upstream release. There is no more a _man file, all manpages
    are in _src debian/rules changed as consequence This partially fixes
    #181557 but project.1 is still a bit weird.
  * Revised man page installation in debian/rules, due to changes in the
    building system.
  * In debian/rules: added LDFLAGS="-g" at build-time, in order to
    create unstripped binaries.
    (closes: #437047)
  * In debian/rules: added private libraries path to search for libs when
    calling dh_shlibdeps
  * Policy bumped to 3.7.3, without changes.
  * Added Vcs-* fields to debian/control.
  * Now installs some additional pdfs in gmt-doc-pdf.
  * GMT wrapper changed to modify PATH before calling a GMT command.
    Patch applied on fly in debian/rules.
    (closes: #416869)
  * Fixed patches and debian/rules to use quilt in top dir and support
    next generation source package.
    (closes: #485163)

 -- Francesco Paolo Lovergine <frankie@debian.org>  Thu, 12 Jun 2008 14:21:53 +0200

gmt (4.2.0-1) unstable; urgency=low

  [ Torsten Landschoff ]
  * New upstream release.
    (closes: #403838, #157744)
  * Hand over to Debian GIS team for group maintenance.

  [ Francesco Paolo Lovergine ]
  * Moved to policy 3.7.2, without changes
  * Debhelper level moved to 5
  * Added html/pdf browser suggestions.
  * Updated home page in copyright file.
  * Added home page ini debian/control descriptions.
    (closes: #416766)
  * Introduced Homepage field in debian/control.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Wed, 03 Oct 2007 13:19:04 +0200

gmt (4.1.2-1.1) unstable; urgency=low

  * Non-maintainer upload to fix Failure To Build From Source due to missing
    build-dependencies.  Thanks to Neil Williams for the patch.
    (Closes: #379214)
  * debian/control: added libxmu-headers and libxaw7-dev as build-dependencies

 -- Margarita Manterola <marga@debian.org>  Thu, 10 Aug 2006 18:26:59 -0300

gmt (4.1.2-1) unstable; urgency=low

  * Correct upstream release (closes: #369216).
  * Include README.Debian file into gmt package (closes: #369215).
  * Include upstream changelog (closes: #369217).
  * Actually include examples in gmt-examples package (closes: #369214).

 -- Torsten Landschoff <torsten@debian.org>  Sat,  3 Jun 2006 02:48:16 +0200

gmt (4.1-1) unstable; urgency=low

  * New upstream release.
  * Use gcc instead of ld for linking libraries (closes: #361043, #352227).
  * Fix package descriptions (closes: #209514, #209578, #209582).
  * Add a README file about getting coastline data.
  * Add build-depends for arch-independent build (closes: #189727, #189728,
    #189730).
  * debian/rules: Stop dh_compress from compressing PDF files.

 -- Torsten Landschoff <torsten@debian.org>  Fri, 26 May 2006 20:31:29 +0200

gmt (4.0-2) unstable; urgency=low

  * debian/control: Make gmt suggest/recommend all other gmt packages
    (closes: #249369).
  * debian/control: Add build dependency on bzip2 (closes: #326842).
    Kudos to Andreas Jochens.

 -- Torsten Landschoff <torsten@debian.org>  Tue,  6 Sep 2005 09:04:17 +0200

gmt (4.0-1) unstable; urgency=low

  * New upstream release.
  * New packaging.

 -- Torsten Landschoff <torsten@debian.org>  Sun,  4 Sep 2005 20:40:54 +0200

gmt (3.4.4-1) unstable; urgency=low

  * New upstream release.

 -- Torsten Landschoff <torsten@debian.org>  Wed, 28 Apr 2004 10:27:45 +0200

gmt (3.4.3-1) unstable; urgency=low

  * New upstream release (closes: #137446, #179809).
  * debian/conffiles: Marked /etc/gmt/{coastline,gmt}.conf as such (lintian).

 -- Torsten Landschoff <torsten@debian.org>  Wed, 12 Nov 2003 02:54:23 +0100

gmt (3.4-2) unstable; urgency=low

  * debian/rules: Fix the condition for running dh_strip so that
    the binaries are stripped if "nostrip" is /NOT/ given *arg*
    (lintian).

 -- Torsten Landschoff <torsten@debian.org>  Sun, 16 Sep 2001 23:11:39 +0200

gmt (3.4-1) unstable; urgency=low

  * New upstream release (closes: #98214). The link to the homepage was
    updated upstream as well (closes: #98204).
  * Applied fix for non-PIC code in libraries (closes: #108806)
    Kudos to LaMont Jones for the patch.
  * debian/copyright: Removed the "Library" before "General Public License"
    which crept in somehow (closes: #106145). Thanks to
    Rafael Labossiere for pointing this out.
  * src/Makefile: Make sure the .lo files are removed during the clean
    phase as well.

 -- Torsten Landschoff <torsten@debian.org>  Fri, 14 Sep 2001 09:44:14 +0200

gmt (3.3.5-2) unstable; urgency=low

  * debian/control: Added missing build-dependency on debhelper
    (closes: #69102)
  * debian/rules: Obey DEB_BUILD_OPTIONS (policy).
  * debian/control: Upgraded Standards-Version to 3.2.0.

 -- Torsten Landschoff <torsten@debian.org>  Mon, 14 Aug 2000 12:03:51 +0200

gmt (3.3.5-1) unstable; urgency=low

  * New upstream release (closes: #62686).
  * Undone the changes to the file searching code I did in the last
    release to fit GMT into Debian policy. Instead I am now using a
    link /usr/share/gmt/share, which links to ".". Therefore you
    can now set $GMTHOME as when compiling by hand (closes: #57485).
  * Included errata from the GMT homepage as of 2000/08/04.

 -- Torsten Landschoff <torsten@debian.org>  Fri, 11 Aug 2000 02:34:53 +0200

gmt (3.3.3-3) unstable frozen; urgency=low

  * Richard: This are only bugfixes from upstream. I am quite sure it will
    not break anything but in case you don't want them for potato I don't
    see a big problem ;)
  * Included bugfixes from http://www.soest.hawaii.edu/gmt/gmt/gmt_bugs.html
    (closes: #56868).
  * debian/README.Debian: Added comments about the changes to the directory
    layout I made and that you should not set GMTHOME (not reported as a bug
    but I got a notice by mail).

 -- Torsten Landschoff <torsten@debian.org>  Thu,  3 Feb 2000 19:05:27 +0100

gmt (3.3.3-2) unstable; urgency=high

  * debian/rules: Fixed compilation so that coastline data is searched
    in /usr/share/gmt (and installed there of course).
  * debian/coastline.conf: Added the path where coastline data was installed
    in older versions.

 -- Torsten Landschoff <torsten@debian.org>  Fri, 21 Jan 2000 13:42:42 +0100

gmt (3.3.3-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Moved package into the science section (closes: #51248).
  * debian/control: Added Build-Depends and moved to policy 3.1.0
  * debian/README: Added information about current state of affairs
    regarding coastline data in Debian.
  * debian/rules: Build system has changed upstream. Adjusted rules file
    accordingly.
  * Added README.coastlines which explains how to install coastline data
    manually.
  * Added mirrors file with the list of known mirrors.
  * debian/rules: Removed configure generated files in clean target.
  * Included erratas from http://imina.soest.hawaii.edu/gmt/gmt/gmt_bugs.html.
  * debian/rules: Override LDFLAGS from configure which sets rpath.

 -- Torsten Landschoff <torsten@debian.org>  Fri, 14 Jan 2000 19:13:26 +0100

gmt (3.3.2-4) unstable; urgency=low

  * Included bugfixes from the GMT homepage.

 -- Torsten Landschoff <torsten@debian.org>  Thu,  4 Nov 1999 17:31:39 +0100

gmt (3.3.2-3) unstable; urgency=low

  * Added reasoning for splitting the manpage package (closes: #47629).

 -- Torsten Landschoff <torsten@debian.org>  Mon,  1 Nov 1999 14:15:09 +0100

gmt (3.3.2-2) unstable; urgency=low

  * Added example coastline.conf with internal documentation (closes: #47627)
  * Moved gmt.conf to /etc/gmt/ where a config file belongs.
  * *arg* I did not notice that Source-Version expands to upstream-debian.
    Changed it in this version but the old one will not work with a new
    manpages package.

 -- Torsten Landschoff <torsten@debian.org>  Mon, 18 Oct 1999 23:22:59 +0200

gmt (3.3.2-1) unstable; urgency=low

  * Initial release.

 -- Torsten Landschoff <torsten@debian.org>  Wed, 11 Aug 1999 20:20:35 +0200
